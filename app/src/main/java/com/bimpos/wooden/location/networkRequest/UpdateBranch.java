package com.bimpos.wooden.location.networkRequest;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.bimpos.wooden.location.BuildConfig;
import com.bimpos.wooden.location.helpers.Constant;
import com.bimpos.wooden.location.models.Branches;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class UpdateBranch extends AsyncTask<String, String, String> {

    HttpURLConnection conn;
    URL url = null;
    private final SharedPreferences prefs;
    private final OnUpdateComplete listener;
    private static final String TAG = "UpdateBranches";
    private final String data;

    public interface OnUpdateComplete {
        void onUpdateComplete(int status);
    }


    public UpdateBranch(Context context, OnUpdateComplete listener, String data) {
        this.data = data;
        this.listener = listener;
        prefs = context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            url = new URL(Constant.BASE_URL + "branches?data=" + data);
            conn = (HttpURLConnection) url.openConnection();
            String token = prefs.getString(Constant.TOKEN, "");
            conn.setRequestProperty("Authorization", "Bearer " + token);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestMethod("POST");
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.connect();

            int response_code = conn.getResponseCode();
            if (response_code == HttpURLConnection.HTTP_OK) {
                InputStream input = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                return (result.toString());
            } else if (response_code == HttpURLConnection.HTTP_UNAUTHORIZED) {
                return "token";
            } else {
                return "notFound";
            }
        } catch (Exception e) {
            return "exception";
        } finally {
            conn.disconnect();
        }
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onPostExecute(String result) {
        Log.d(TAG, "onPostExecute: result " + result);
        if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("notFound")) {
            listener.onUpdateComplete(0);
        } else if (result.equalsIgnoreCase("token")) {
            listener.onUpdateComplete(-1);
        } else {
            listener.onUpdateComplete(1);
        }
    }
}
