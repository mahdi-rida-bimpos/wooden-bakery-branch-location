package com.bimpos.wooden.location.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.bimpos.wooden.location.BuildConfig;
import com.bimpos.wooden.location.R;
import com.bimpos.wooden.location.helpers.Constant;
import com.bimpos.wooden.location.helpers.NetworkUtil;
import com.bimpos.wooden.location.networkRequest.GetBranches;
import com.bimpos.wooden.location.networkRequest.GetToken;

public class MainActivity extends AppCompatActivity implements GetToken.OnTokenReceived, GetBranches.OnBranchesComplete {

    private EditText userName, password;
    private CheckBox remember;
    private SharedPreferences prefs;
    private Button login;

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(TAG, "onCreate: start");
        initVariables();
        checkRemember();
        checkPermission();

        login.setOnClickListener(view -> {
            String userNameText = userName.getText().toString();
            String passwordText = password.getText().toString();
            if (TextUtils.isEmpty(userNameText) || TextUtils.isEmpty(passwordText)) {
                Toast.makeText(MainActivity.this, "Please write your user name", Toast.LENGTH_SHORT).show();
            } else {
                Constant.userName = userNameText;
                Constant.password = passwordText;
                if (remember.isChecked()) {
                    prefs.edit().putString("username", userNameText).apply();
                    prefs.edit().putString("password", passwordText).apply();
                }else{
                    prefs.edit().remove("username").apply();
                    prefs.edit().remove("password").apply();
                }
                if (checkConnection()) {
                    getBranches();
                } else {
                    showNoInternetDialog();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void checkRemember() {
        String name = prefs.getString("username", "");
        String pass = prefs.getString("password", "");
        if (name.length() != 0 && pass.length() != 0) {
            remember.setChecked(true);
            userName.setText(name);
            password.setText(pass);
        }
    }

    private void initVariables() {
        login = findViewById(R.id.activity_main_login);
        userName = findViewById(R.id.activity_main_userName);
        password = findViewById(R.id.activity_main_password);
        remember = findViewById(R.id.activity_main_remember);
        prefs = getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
    }

    private void showNoInternetDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Note");
        builder.setMessage("Please make sure to have an internet connection");
        builder.setPositiveButton("Exit", (dialogInterface, i) -> finish());
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private boolean checkConnection() {
        return NetworkUtil.getConnectivityStatusString(this);
    }

    private void getBranches() {
        GetBranches getBranches = new GetBranches(this, this);
        getBranches.execute();
    }

    @Override
    public void onBranchesComplete(int status) {
        if (status == -1) {
            getToken();
        } else if (status == 0) {
            showError();
        } else if (status == 1) {
            gotoBranchesActivities();
        }
    }

    private void showError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Note");
        builder.setMessage("Something went wrong, please try again in few moments");
        builder.setPositiveButton("Exit", (dialogInterface, i) -> finish());
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void gotoBranchesActivities() {
        startActivity(new Intent(this, BranchesActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    private void getToken() {
        GetToken getToken = new GetToken(this, this);
        getToken.execute();
    }

    @Override
    public void onTokenReceived(int status) {
        if (status == 1) {
            getBranches();
        } else {
            showError();
        }
    }


    protected void checkPermission() {
        Log.d(TAG, "checkPermission: start");
        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION)
                + ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Location" +
                        " permission is required .");
                builder.setTitle("Please grant this permission");
                builder.setPositiveButton("OK", (dialogInterface, i) -> ActivityCompat.requestPermissions(
                        MainActivity.this,
                        new String[]{
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION
                        },
                        1
                ));
                builder.setNeutralButton("Cancel", (dialog, which) -> finish());
                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                // Directly request for required permissions, without explanation
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION
                        },
                        1
                );
            }
        }
    }

}