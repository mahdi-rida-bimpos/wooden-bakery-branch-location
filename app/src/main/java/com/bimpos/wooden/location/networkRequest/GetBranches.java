package com.bimpos.wooden.location.networkRequest;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.bimpos.wooden.location.BuildConfig;
import com.bimpos.wooden.location.helpers.Constant;
import com.bimpos.wooden.location.models.Branches;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GetBranches extends AsyncTask<String, String, String> {

    HttpURLConnection conn;
    URL url = null;
    private final SharedPreferences prefs;
    private final OnBranchesComplete listener;
    private static final String TAG = "GetBranches";

    public interface OnBranchesComplete {
        void onBranchesComplete(int status);
    }


    public GetBranches(Context context, OnBranchesComplete listener) {
        this.listener = listener;
        prefs = context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            url = new URL(Constant.BASE_URL + "branches");
            conn = (HttpURLConnection) url.openConnection();
            String token = prefs.getString(Constant.TOKEN, "");
            conn.setRequestProperty("Authorization", "Bearer " + token);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestMethod("GET");
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.connect();

            int response_code = conn.getResponseCode();
            if (response_code == HttpURLConnection.HTTP_OK) {
                InputStream input = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                return (result.toString());
            } else {
                return ("unsuccessful");
            }
        } catch (Exception e) {
            return "exception";
        } finally {
            conn.disconnect();
        }
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onPostExecute(String result) {
        Log.d(TAG, "onPostExecute: result " + result);
        if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("unsuccessful")) {
            listener.onBranchesComplete(-1);
        } else {
            try {
                List<Branches> branchesList = new ArrayList<>();

                int id, isactive, acceptsdelivery, acceptstakeaway;
                double longitude, latitude;
                String name, address, email, phone, clientid, picpath, openinghours, picture, deliveryhours;

                JSONArray parentArray = new JSONArray(result);

                for (int i = 0; i < parentArray.length(); i++) {
                    JSONObject branchObject = parentArray.getJSONObject(i);
                    id = branchObject.getInt("id");
                    isactive = branchObject.getInt("isactive");
                    acceptsdelivery = branchObject.getInt("acceptsdelivery");
                    acceptstakeaway = branchObject.getInt("acceptstakeaway");
                    name = branchObject.getString("name");
                    address = branchObject.getString("address");
                    email = branchObject.getString("email");
                    phone = branchObject.getString("phone");
                    longitude = branchObject.getDouble("longitude");
                    latitude = branchObject.getDouble("latitude");
                    clientid = branchObject.getString("clientid");
                    picpath = branchObject.getString("picpath");
                    openinghours = branchObject.getString("openinghours");
                    picture = branchObject.getString("picture");
                    deliveryhours = branchObject.getString("deliveryhours");


                    Branches branch = new Branches(id, isactive, acceptsdelivery, acceptstakeaway, deliveryhours,
                            name, address, phone, email, longitude, latitude, clientid, picpath, openinghours, picture);
                    branchesList.add(branch);
                }
                Constant.branchesList = branchesList;
                listener.onBranchesComplete(1);

            } catch (JSONException e) {
                listener.onBranchesComplete(0);
            }
        }
    }
}
