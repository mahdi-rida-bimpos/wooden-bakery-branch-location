package com.bimpos.wooden.location.networkRequest;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;


import com.bimpos.wooden.location.BuildConfig;
import com.bimpos.wooden.location.helpers.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class GetToken extends AsyncTask<String, String, String> {

    private final String TAG = "GetToken";
    private final OnTokenReceived listener;
    HttpURLConnection conn;
    private final SharedPreferences prefs;
    URL url = null;

    public interface OnTokenReceived {
        void onTokenReceived(int status);
    }

    public GetToken(OnTokenReceived listener, Context context) {
        this.listener = listener;
        prefs = context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
    }

    @Override
    protected String doInBackground(String... params) {

        try {
            String username = Constant.userName;
            String password = Constant.password;
            Log.d(TAG, "doInBackground: user "+username);
            Log.d(TAG, "doInBackground: password "+password);
            url = new URL(Constant.BASE_URL + "login");
            Log.d(TAG, "doInBackground: url "+url);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(5000);
            conn.setConnectTimeout(5000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("username", username)
                    .appendQueryParameter("password", password);

            String query = builder.build().getEncodedQuery();
            OutputStream outputStream = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(outputStream, StandardCharsets.UTF_8));
            writer.write(query);
            writer.flush();
            writer.close();
            outputStream.close();

            conn.connect();

            int response_code = conn.getResponseCode();
            Log.d(TAG, "doInBackground: code "+response_code);
            if (response_code == HttpURLConnection.HTTP_OK) {

                InputStream input = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuilder result = new StringBuilder();
                String line;

                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                return (result.toString());

            } else {
                return ("unsuccessful");
            }
        } catch (Exception e) {
            Log.d(TAG, "doInBackground: error " + e.getMessage());
            return "exception";
        } finally {
            conn.disconnect();
        }
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onPostExecute(String result) {
        Log.d(TAG, "onPostExecute: result " + result);
        if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("unsuccessful")) {
            listener.onTokenReceived(-1);
        } else {
            try {
                JSONObject object = new JSONObject(result);
                String token = object.getString("access_token");
                String tokenType = object.getString("token_type");
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(Constant.TOKEN, token);
                editor.putString(Constant.TOKEN_TYPE, tokenType);
                editor.apply();

                if (listener != null) {
                    listener.onTokenReceived(1);
                }

            } catch (JSONException e) {
                listener.onTokenReceived(-1);
            }
        }
    }
}