package com.bimpos.wooden.location.adapters;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


import com.bimpos.wooden.location.R;
import com.bimpos.wooden.location.activities.BranchesActivity;
import com.bimpos.wooden.location.activities.BranchesDetailsActivity;
import com.bimpos.wooden.location.models.Branches;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class BranchesRvAdapter extends RecyclerView.Adapter<BranchesRvAdapter.ViewHolder> implements Filterable {

    private final List<Branches> branchList;
    private List<Branches> filteredList;
    private final Context context;
    private final BranchesActivity activity;
    private long mLastClick = 0;

    public BranchesRvAdapter(List<Branches> branchList, Context context, BranchesActivity activity) {
        this.branchList = branchList;
        this.activity = activity;
        this.filteredList = branchList;
        this.context = context;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    filteredList = branchList;
                } else {
                    List<Branches> newFilteredList = new ArrayList<>();
                    for (Branches branches : branchList) {
                        if (branches.getName().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                            newFilteredList.add(branches);
                        }
                    }
                    filteredList = newFilteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @SuppressLint("NotifyDataSetChanged")
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredList = (ArrayList<Branches>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_branch, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Branches branch = filteredList.get(position);

        Picasso.get().load(branch.getPicPath())
                .placeholder(R.drawable.wooden_branch)
                .into(holder.branchImage);

        holder.branchName.setText(branch.getName());
        holder.openingHours.setText(branch.getOpeningHours());
        holder.phone.setText(branch.getPhone());

        if (branch.getLatitude() != 0 || branch.getLongitude() != 0) {
            holder.mapBtn.setOnClickListener(v -> {

                if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                    return;
                }
                mLastClick = SystemClock.elapsedRealtime();

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:<" + branch.getLatitude() + ">,<" + branch.getLongitude() + ">?q=<" + branch.getLatitude() + ">,<" + branch.getLongitude() + ">(" + branch.getName() + ")"));
                context.startActivity(intent);
            });
        } else {
            holder.mapBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.branch_map_grey_icon));
        }


        if (branch.getPhone().length() != 0) {

            if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();

            holder.callBtn.setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + branch.getPhone() + ""));
                context.startActivity(intent);

            });
        } else {
            holder.callBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.branch_call_grey_icon));
        }


        if (branch.getEmail().length() != 0) {
            holder.emailBtn.setOnClickListener(v -> {

                if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                    return;
                }
                mLastClick = SystemClock.elapsedRealtime();

                Intent i = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", branch.getEmail(), null));
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, "");
                context.startActivity(Intent.createChooser(i, "Send email"));
            });
        } else {
            holder.emailBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.branch_email_grey_icon));
        }


        holder.mainLayout.setOnClickListener(v -> {

            if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();

            Intent intent = new Intent(context, BranchesDetailsActivity.class).putExtra(Branches.class.getSimpleName(), branch);
            ActivityOptions options = ActivityOptions
                    .makeSceneTransitionAnimation(activity, holder.mainLayout, "activity_branches_details_mainLayout");
            // start the new activity
            activity.startActivity(intent, options.toBundle());
        });

        holder.mainLayout.setOnLongClickListener(v -> {

            if (SystemClock.elapsedRealtime() - mLastClick < 1000) {
                return false;
            }
            mLastClick = SystemClock.elapsedRealtime();

            Intent intent = new Intent(context, BranchesDetailsActivity.class).putExtra(Branches.class.getSimpleName(), branch);
            ActivityOptions options = ActivityOptions
                    .makeSceneTransitionAnimation(activity, holder.mainLayout, "activity_branches_details_mainLayout");
            // start the new activity
            activity.startActivity(intent, options.toBundle());
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView branchName, openingHours, isOpenText, phone;
        ImageView emailBtn, mapBtn, callBtn, branchImage;
        LinearLayout mainLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            branchName = itemView.findViewById(R.id.recycler_branch_name);
            openingHours = itemView.findViewById(R.id.recycler_branch_openingHours);
            isOpenText = itemView.findViewById(R.id.recycler_branch_isOpenText);
            phone = itemView.findViewById(R.id.recycler_branch_phone);
            emailBtn = itemView.findViewById(R.id.recycler_branch_emailBtn);
            mapBtn = itemView.findViewById(R.id.recycler_branch_mapBtn);
            callBtn = itemView.findViewById(R.id.recycler_branch_callBtn);
            branchImage = itemView.findViewById(R.id.recycler_branch_image);
            mainLayout = itemView.findViewById(R.id.recycler_branch_mainLayout);
        }
    }
}
