package com.bimpos.wooden.location.activities;

import static com.bimpos.wooden.location.helpers.Constant.branchesList;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bimpos.wooden.location.R;
import com.bimpos.wooden.location.databinding.ActivityBranchesDetailsBinding;
import com.bimpos.wooden.location.models.Branches;
import com.bimpos.wooden.location.networkRequest.GetToken;
import com.bimpos.wooden.location.networkRequest.UpdateBranch;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;


public class BranchesDetailsActivity extends AppCompatActivity implements
        OnMapReadyCallback, UpdateBranch.OnUpdateComplete, GetToken.OnTokenReceived {

    private static final String TAG = "BranchesDetailsActivity";
    private ActivityBranchesDetailsBinding binding;
    private static Branches branch;
    private final int DIALOG_ERROR = 1;
    private final int DIALOG_UPDATE_DONE = 2;
    private GoogleMap map;
    private FusedLocationProviderClient fusedLocationClient;
    private LatLng branchLocation = null;
    private boolean canClick = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBranchesDetailsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        checkIntent();
        initClickListener();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: start");
        checkGps();
    }

    private void checkGps() {
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }
        if (!gps_enabled && !network_enabled) {
            // notify user
            new AlertDialog.Builder(this)
                    .setTitle("Location request")
                    .setMessage("Please turn your location service on")
                    .setPositiveButton("settings", (paramDialogInterface, paramInt) -> startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)))
                    .setNegativeButton("cancel", (dialog, which) -> finish())
                    .show();
        } else {
            initMapFragment();
        }
    }

    private void checkIntent() {
        branch = (Branches) getIntent().getSerializableExtra(Branches.class.getSimpleName());
        if (branch == null) {
            finish();
        } else {
            fillContent();
        }
    }

    private void fillContent() {
        Log.d(TAG, "fillContent: branch " + branch.toString());
        binding.name.setText(branch.getName());
        binding.openingHours.setText(branch.getOpeningHours());
        binding.phone.setText(branch.getPhone());

        if (branch.getLongitude() != 0 || branch.getLatitude() != 0) {
            binding.mapBtn.setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:<" + branch.getLatitude() + ">,<" + branch.getLongitude() + ">?q=<" + branch.getLatitude() + ">,<" + branch.getLongitude() + ">(" + branch.getName() + ")"));
                startActivity(intent);
            });
        } else {
            binding.mapBtn.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.branch_map_grey_icon));
        }

        if (branch.getPhone().length() != 0) {
            binding.callBtn.setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + branch.getPhone() + ""));
                startActivity(intent);

            });
        } else {
            binding.callBtn.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.branch_call_grey_icon));
        }


        if (branch.getEmail().length() != 0) {
            binding.emailBtn.setOnClickListener(v -> {
                Intent i = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", branch.getEmail(), null));
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(i, "Send email"));
            });
        } else {
            binding.emailBtn.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.branch_email_grey_icon));
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        supportFinishAfterTransition();
    }

    private void initVariables() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }

    private void initClickListener() {
        binding.back.setOnClickListener(view -> onBackPressed());

        binding.updateLocation.setOnClickListener(v -> {
            if (canClick) {
                canClick = false;
                updateLocation();
            }
        });

    }

    private void updateLocation() {

        binding.progressBar.setVisibility(View.VISIBLE);
        try {
            JSONObject branchObject = new JSONObject();
            branchObject.put("id", branch.getId());
            branchObject.put("clientid", branch.getClientId());
            branchObject.put("name", branch.getName());
            branchObject.put("address", branch.getAddress());
            branchObject.put("phone", branch.getPhone());
            branchObject.put("email", branch.getEmail());
            branchObject.put("openinghours", branch.getOpeningHours());
            branchObject.put("longitude", branch.getLongitude());
            branchObject.put("latitude", branch.getLatitude());
            branchObject.put("isactive", branch.getIsActive());
            branchObject.put("acceptsdelivery", branch.getAcceptsDelivery());
            branchObject.put("acceptstakeaway", branch.getAcceptsTakeAway());

            UpdateBranch updateBranch = new UpdateBranch(this, this, branchObject.toString());
            updateBranch.execute();

        } catch (JSONException e) {
            showErrorDialog("Error while sending data", 1);
        }
    }

    @Override
    public void onUpdateComplete(int status) {
        if (status == -1) {
            GetToken getToken = new GetToken(this, this);
            getToken.execute();

        } else if (status == 0) {
            showErrorDialog("Error while updating this branch", DIALOG_ERROR);
        } else {
            showErrorDialog("this branch has been updated", DIALOG_UPDATE_DONE);
        }
    }

    @Override
    public void onTokenReceived(int status) {

        if (status == -1) {
            showErrorDialog("Error connecting to the server", DIALOG_ERROR);
        } else {
            updateLocation();
        }
    }

    private void showErrorDialog(String message, int action) {
        binding.progressBar.setVisibility(View.GONE);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Note");
        builder.setMessage(message);
        builder.setPositiveButton("OK", (dialog, which) -> {
            switch (action) {
                case DIALOG_ERROR:
                    finish();
                    break;

                case DIALOG_UPDATE_DONE:
                    gotoBranchesActivity();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

    }

    private void gotoBranchesActivity() {
        for (int i = 0; i < branchesList.size(); i++) {
            Branches branches = branchesList.get(i);
            if (branches.getId() == branch.getId()) {
                branchesList.get(i).setLongitude( branch.getLongitude());
                branchesList.get(i).setLatitude(branch.getLatitude());
                break;
            }
        }
        Intent intent = new Intent(this, BranchesActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void initMapFragment() {
        Log.d(TAG, "initMapFragment: start");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.activity_branches_details_mapFragment);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {

        this.map = googleMap;
        binding.progressBar.setVisibility(View.GONE);
        setLocation();
    }

    private void setLocation() {
        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION)
                + ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Location" +
                        " permission is required .");
                builder.setTitle("Please grant this permission");
                builder.setPositiveButton("OK", (dialogInterface, i) -> ActivityCompat.requestPermissions(
                        BranchesDetailsActivity.this,
                        new String[]{
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION
                        },
                        1
                ));
                builder.setNeutralButton("Cancel", (dialog, which) -> finish());
                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                // Directly request for required permissions, without explanation
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION
                        },
                        1
                );
            }
        } else {
            map.setMyLocationEnabled(true);
            if (map != null) {

                if (branch.getLatitude() != 0 && branch.getLongitude() != 0) {
                    branchLocation = new LatLng(branch.getLatitude(), branch.getLongitude());
                    map.addMarker(new MarkerOptions().position(branchLocation));
                }

                fusedLocationClient.getLastLocation().addOnSuccessListener(this, location -> {
                    if (location != null) {
                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        map.animateCamera(CameraUpdateFactory.zoomTo(17.0f));
                        if (branchLocation == null) {
                            map.addMarker(new MarkerOptions().position(latLng));
                        }
                    }
                });

                map.setOnMapClickListener(latLng -> {
                    map.clear();
                    Log.d(TAG, "setLocation: latitude " + latLng.latitude);
                    Log.d(TAG, "setLocation: longitude " + latLng.longitude);
                    branchLocation = latLng;
                    branch.setLongitude(branchLocation.longitude);
                    branch.setLatitude(branchLocation.latitude);
                    map.addMarker(new MarkerOptions().position(latLng));
                });

            }
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if ((grantResults.length > 0) && (grantResults[0] + grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                setLocation();
            }
        }
    }


}