package com.bimpos.wooden.location.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;


import com.bimpos.wooden.location.adapters.BranchesRvAdapter;
import com.bimpos.wooden.location.databinding.ActivityBranchesBinding;
import com.bimpos.wooden.location.helpers.Constant;
import com.bimpos.wooden.location.models.Branches;

import java.util.List;
import java.util.Objects;

public class BranchesActivity extends AppCompatActivity {

    private ActivityBranchesBinding binding;
    private List<Branches> branchList;
    private BranchesRvAdapter adapter;
    private InputMethodManager imm;

    private static final String TAG = "BranchesActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBranchesBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Log.d(TAG, "onCreate: start");
        initVariables();
        initRecyclerView();
        initClickListener();
        initSearchView();
        checkPermission();
    }

    protected void checkPermission() {
        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION)
                + ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    ||ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Location" +
                        " permission is required .");
                builder.setTitle("Please grant this permission");
                builder.setPositiveButton("OK", (dialogInterface, i) -> ActivityCompat.requestPermissions(
                        BranchesActivity.this,
                        new String[]{
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION
                        },
                        1
                ));
                builder.setNeutralButton("Cancel", (dialog, which) -> finish());
                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                // Directly request for required permissions, without explanation
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION
                        },
                        1
                );
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {
            if ((grantResults.length > 0) && (grantResults[0] + grantResults[1] != PackageManager.PERMISSION_GRANTED)) {
                finish();
            }
        }
    }

    private void initSearchView() {
        binding.searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (Objects.requireNonNull(binding.searchText.getText()).toString().length() != 0  || binding.searchText.isFocused()) {
            binding.searchText.setText("");
            binding.searchText.clearFocus();
            imm.hideSoftInputFromWindow(binding.searchText.getWindowToken(), 0);
            return;
        }

        super.onBackPressed();
    }

    private void initVariables() {
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        branchList = Constant.branchesList;
    }

    private void initRecyclerView() {
        adapter = new BranchesRvAdapter(branchList, this,this);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(adapter);

        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                binding.searchText.clearFocus();
                imm.hideSoftInputFromWindow(binding.searchText.getWindowToken(), 0);
            }
        });
    }

    private void initClickListener() {
        binding.back.setOnClickListener(view -> onBackPressed());
    }

}